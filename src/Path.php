<?php

/**
 * Some utilities for dealing with paths, files and directories
 */
namespace Skript\Utils\Path;

const DS = DIRECTORY_SEPARATOR;

/**
 * Creates a symlink to a specific path, at a specific location
 *
 * @param string The path to link to
 * @param string The location at which to place the link
 */
function symlink(string $targetPath, string $linkPath = null): void
{
    $cwd = getcwd();
    $absTarget = makeAbsolute($targetPath, $cwd);
    if ($linkPath) {
        $absLink = makeAbsolute($linkPath, $cwd);
        $linkName = basename($absLink);
    } else {
        $linkName = basename($absTarget);
        $absLink = $cwd.DS.$linkName;
    }

    if (file_exists($absLink)) {
        throw new \Exception("Failed to create symlink '$linkPath': file exists");
        return;
    }

    $relTargetPath = getRelativePath(dirname($absLink), $absTarget);

    chdir(dirname($absLink));
    \symlink($relTargetPath, $linkName);
    chdir($cwd);
}

/**
 * Recursively creates a directory and fails silently when it already exists.
 * Similar to `mkdir -p`
 */
function mkdirAndParents(string $dir, int $mode = 0755): bool
{
    return !file_exists($dir) && mkdir($dir, $mode, true);
}

/**
 * Recursively removes a path and fails silently when it does not exists.
 * Similar to `rm -rf`
 */
function rmrf(string $path): bool
{
    // delete regular files and symlinks
    if (!is_dir($path) || is_link($path)) {
        return @unlink($path);
    }
    // recursively delete files and directories
    $files = array_diff(scandir($path), ['.', '..']);
    foreach ($files as $file) {
        rmrf($path.DS.$file);
    }
    return rmdir($path);
}

/**
 * Returns a relative path pointing from a source to a target
 */
function getRelativePath(string $srcPath, string $targetPath): ?string
{
    if (!isAbsolute($srcPath)) {
        throw new \Exception('Source path needs to be an absolute path, relative path "'.$srcPath.'" given.');
    }
    if (!isAbsolute($targetPath)) {
        throw new \Exception('Target path needs to be an absolute path, relative path "'.$targetPath.'" given.');
    }
    $srcParts = explode(DS, normalize($srcPath));
    $targetParts = explode(DS, normalize($targetPath));

    // root of paths do not match
    if ($srcParts[1] !== $targetParts[1]) {
        return null;
    }

    $offset = 0;
    $relPath = '';
    foreach ($srcParts as $i => $dir) {
        if (isset($targetParts[$i]) && $srcParts[$i] === $targetParts[$i]) {
            $offset++;
            continue;
        }
        $relPath .= '..'.DS;
    }

    return $relPath . implode(DS, array_slice($targetParts, $offset));
}

function getRelativePaths(string $srcPath, array $targetPaths): array
{
    $relPaths = [];
    foreach ($targetPaths as $i => $path) {
        $relPath = getRelativePath($srcPath, $path);
        if ($relPath !== null) {
            $relPaths[$i] = $relPath;
        }
    }
    return $relPaths;
}

function makeAbsolute(string $path, string $absPath = DS): string
{
    if (strpos($path, DS) !== 0) {
        $path = rtrim($absPath, DS).DS.$path;
    }
    return $path;
}

function normalize(string $path): string
{
    // strip out single dot symbols
    $path = str_replace('/\./', DS, $path);
    $path = preg_replace('|^\.'.DS.'|', '', $path);
    $path = preg_replace('|'.DS.'\.$|', '', $path);

    // resolve double dot symbols
    if (strpos($path, '..') > -1) {
        $parts = explode(DS, $path);
        $newPath = [];
        // retain dots at the beginning
        $newParts = [];
        $startPos=0;
        foreach ($parts as $i => $part) {
            if ($i==$startPos && $part == '..') {
                $startPos=$i+1;
                continue;
            }
            $newParts[] = $part;
        }
        // resolve the rest of the part
        foreach ($newParts as $i => $part) {
            if ($part === '..') {
                unset($newPath[count($newPath)-1]);
                continue;
            }
            $newPath[] = $part;
        }
        $path = implode(
            DS,
            array_merge(
                array_fill(0, $startPos, '..'), // recreate start parts
                $newPath // append resolved path
            )
        );
    }

    return rtrim($path, DS);
}

function isRelative(string $path): bool
{
    return !isAbsolute($path);
}

function isAbsolute(string $path): bool
{
    return $path[0] === DS;
}
